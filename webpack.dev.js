const Path = require('path')
const merge = require('webpack-merge')
const webpackCommon = require('./webpack.config')

module.exports = merge(webpackCommon, {
    mode: 'development',
    devtool: 'eval-source-map',
    devServer: {
        inline: true,
        hot: true,
        contentBase: [Path.resolve('./build')],
        watchContentBase: true,
        open: true,
        compress: true,
        writeToDisk: true,
        watchOptions: {
            poll: true,
        },
    },
})
