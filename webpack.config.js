const path = require('path')
const webpack = require('webpack')
const { styles, bundler } = require('@ckeditor/ckeditor5-dev-utils')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const DEV_MODE = process.env.npm_lifecycle_event == 'start'
const CKEditorWebpackPlugin = require('@ckeditor/ckeditor5-dev-webpack-plugin')

module.exports = {
    entry: path.resolve(__dirname, 'src/main.js'),
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'build'),
        library: 'KlaraCkeditor',
        libraryTarget: 'umd',
        libraryExport: 'default',
    },
    plugins: [
        new CleanWebpackPlugin(),
        new webpack.BannerPlugin({
            banner: bundler.getLicenseBanner(),
            raw: true,
        }),
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'public/index.html'),
        }),
        new MiniCssExtractPlugin({
            filename: `[name]${DEV_MODE ? '' : '.min'}.css`,
        }),
        new CKEditorWebpackPlugin({
            // UI language. Language codes follow the https://en.wikipedia.org/wiki/ISO_639-1 format.
            // When changing the built-in language, remember to also change it in the editor configuration (src/ckeditor.js).
            language: 'en',
            additionalLanguages: 'all',
        }),
        new webpack.BannerPlugin({
            banner: [
                'CKEditor 5 with collaboration features is licensed only under a commercial license and is protected by copyright law.',
                'For more details about available licensing options please contact us at' + ' https://ckeditor.com/contact/.',
            ].join('\n'),
        }),
    ],
    module: {
        rules: [
            {
                test: /\.svg$/,
                use: ['raw-loader'],
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                        options: {
                            injectType: 'singletonStyleTag',
                            attributes: {
                                'data-cke': true,
                            },
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: styles.getPostCssConfig({
                            themeImporter: {
                                themePath: require.resolve('@ckeditor/ckeditor5-theme-lark'),
                            },
                            minify: true,
                        }),
                    },
                ],
            },
        ],
    },
}
