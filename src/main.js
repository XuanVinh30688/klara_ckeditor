import ClassicEditorBase from '@ckeditor/ckeditor5-editor-classic/src/classiceditor'
import InlineEditorBase from '@ckeditor/ckeditor5-editor-inline/src/inlineeditor'
import FontColorCustom from './custom-font-color/custom-font-color'
import CustomLink from './custom-link/custom-link'

import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment.js'
import Autosave from '@ckeditor/ckeditor5-autosave/src/autosave.js'
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold.js'
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials.js'
import FontBackgroundColor from '@ckeditor/ckeditor5-font/src/fontbackgroundcolor.js'
import FontColor from '@ckeditor/ckeditor5-font/src/fontcolor.js'
import FontFamily from '@ckeditor/ckeditor5-font/src/fontfamily.js'
import FontSize from '@ckeditor/ckeditor5-font/src/fontsize.js'
import Heading from '@ckeditor/ckeditor5-heading/src/heading.js'
import Highlight from '@ckeditor/ckeditor5-highlight/src/highlight.js'
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic.js'
import Link from '@ckeditor/ckeditor5-link/src/link.js'
import List from '@ckeditor/ckeditor5-list/src/list.js'
import ListStyle from '@ckeditor/ckeditor5-list/src/liststyle.js'
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph.js'
import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough.js'
import Table from '@ckeditor/ckeditor5-table/src/table.js'
import TableCellProperties from '@ckeditor/ckeditor5-table/src/tablecellproperties'
import TableProperties from '@ckeditor/ckeditor5-table/src/tableproperties'
import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar.js'
import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline.js'

class ClassicEditor extends ClassicEditorBase {}
class InlineEditor extends InlineEditorBase {}

const plugins = [
    Alignment,
    Autosave,
    Bold,
    Essentials,
    FontBackgroundColor,
    FontFamily,
    FontSize,
    Heading,
    Highlight,
    Italic,
    Link,
    List,
    ListStyle,
    Paragraph,
    Strikethrough,
    Table,
    TableCellProperties,
    TableProperties,
    TableToolbar,
    Underline,
    CustomLink,
    FontColorCustom
]

ClassicEditor.builtinPlugins = plugins
InlineEditor.builtinPlugins = plugins

const defaultConfig = {
    toolbar: {
        items: [
            'bold',
            'italic',
            'underline',
            'strikethrough',
            'link',
            '|',
            'heading',
            'alignment',
            'bulletedList',
            'numberedList',
            '|',
            'insertTable',
            '|',
            'fontBackgroundColor',
            'fontColor',
            'fontSize',
            'fontFamily',
            'highlight',
            'undo',
            'redo',
        ],
    },
    image: {
        toolbar: ['imageStyle:full', 'imageStyle:side', '|', 'imageTextAlternative'],
    },
    table: {
        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells', 'tableCellProperties', 'tableProperties'],
    },
}

ClassicEditor.defaultConfig = defaultConfig
InlineEditor.defaultConfig = defaultConfig

export default {
    ClassicEditor,
    InlineEditor
}
