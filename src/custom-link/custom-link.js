import LinkEditing from '@ckeditor/ckeditor5-link/src/linkediting'
import AutoLink from '@ckeditor/ckeditor5-link/src/autolink'
import Plugin from '@ckeditor/ckeditor5-core/src/plugin'

import CustomlinkUI from './custom-linkui'
import UnLink from './custom-unlink'

export default class CustomLink extends Plugin {
    static get requires() {
        return [LinkEditing, CustomlinkUI, AutoLink, UnLink]
    }

    static get pluginName() {
        return 'CustomLink'
    }
}
