import { addLinkProtocolIfApplicable } from '@ckeditor/ckeditor5-link/src/utils'
import LinkUI from '@ckeditor/ckeditor5-link/src/linkui'
import ClickObserver from '@ckeditor/ckeditor5-engine/src/view/observer/clickobserver'
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview'
import linkIcon from '@ckeditor/ckeditor5-link/theme/icons/link.svg'
const VISUAL_SELECTION_MARKER_NAME = 'link-ui'

const TOOBAR_BUTTON_LINK = 'customLink'

const CUSTOM_LINK_EVENTS = {
    CUSTOM_LINK_BTN_CLICKED: 'custom-link-btn-clicked',
    APPLY_CUSTOM_LINK: 'apply-custom-link',
    CANCEL_ADD_LINK: 'cancel-add-link',
}
export default class CustomlinkUI extends LinkUI {
    static get pluginName() {
        return 'CustomLinkUI'
    }

    init() {
        const editor = this.editor

        editor.editing.view.addObserver(ClickObserver)

        // Create toolbar buttons custom.
        this._createToolbarLinkButton()

        // Override lissen click on document event
        this._enableUserBalloonInteractions()

        // Renders a fake visual selection marker on an expanded selection.
        editor.conversion.for('editingDowncast').markerToHighlight({
            model: VISUAL_SELECTION_MARKER_NAME,
            view: {
                classes: ['ck-fake-link-selection', 'data-maker'],
                attributes: {
                    id: 'data-maker',
                },
            },
            converterPriority: 'highest',
        })

        // Renders a fake visual selection marker on a collapsed selection.
        editor.conversion.for('editingDowncast').markerToElement({
            model: VISUAL_SELECTION_MARKER_NAME,
            view: {
                name: 'span',
                classes: ['ck-fake-link-selection', 'ck-fake-link-selection_collapsed', 'data-marker'],
                attributes: {
                    id: 'data-maker',
                },
            },
            converterPriority: 'highest',
        })

        this.bindEvents()
    }

    bindEvents() {
        const editor = this.editor

        editor.on(CUSTOM_LINK_EVENTS.APPLY_CUSTOM_LINK, (eventInfo, value) => this.handleInsertLink(editor, eventInfo, value))
        editor.on(CUSTOM_LINK_EVENTS.CANCEL_ADD_LINK, (eventInfo) => {
            this._hideFakeVisualSelection()
            eventInfo.stop()
        })
    }

    // Override LinkUI methods
    destroy() {
		this.stopListening();
	}

    _createToolbarLinkButton() {
        const editor = this.editor
        const linkCommand = editor.commands.get('link')
        const translate = editor.t
        const editingDocument = editor.editing.view.document

        editor.ui.componentFactory.add(TOOBAR_BUTTON_LINK, (locale) => {
            const button = new ButtonView(locale)

            button.isEnabled = true
            button.label = translate('Custom Link')
            button.icon = linkIcon
            button.tooltip = true;
            button.isToggleable = true
            button.class = 'custom-link'

            // Bind button to the command.
            button
                .bind('isEnabled')
                .to(
                    linkCommand,
                    'value',
                    editingDocument,
                    'isFocused',
                    (isCommandEnabled, isDocumentFocused) => !isCommandEnabled && isDocumentFocused,
                )

            this.listenTo(button, 'execute', () => {
                // Fire event when clicking on the custom link toolbar button
                if (!this._getSelectedLinkElement()) {
                    this._showFakeVisualSelection()

                    const markerViewElements = Array.from(editor.editing.mapper.markerNameToElements(VISUAL_SELECTION_MARKER_NAME))

                    editor.fire(CUSTOM_LINK_EVENTS.CUSTOM_LINK_BTN_CLICKED, markerViewElements)
                }
            })

            return button
        })
    }

    _enableUserBalloonInteractions() {
        const editor = this.editor
        const viewDocument = editor.editing.view.document
        /*
            Prevent to show default Balloon panel when using the custom link
            If using the custom link, this listener will be called and no other listeners are called anymore
        */
        const toolbarItems = editor.config._config.toolbar.items ? editor.config._config.toolbar.items : editor.config._config.toolbar
        const isExitsCustomLink = toolbarItems.indexOf(TOOBAR_BUTTON_LINK) >= 0
        const priority = isExitsCustomLink ? 'high' : 'lowest'

        this.listenTo(
            viewDocument,
            'click',
            (eventInfo) => {
                eventInfo.stop()
            },
            { priority: priority },
        )
    }

    // End Override LinkUI methods

    handleInsertLink(editor, eventInfo, value) {
        if (!value) {
            return
        }

        const defaultProtocol = editor.config.get('link.defaultProtocol')
        const parsedUrl = addLinkProtocolIfApplicable(value, defaultProtocol)

        editor.execute('link', parsedUrl, {})
        this._hideFakeVisualSelection()
        eventInfo.stop()
    }
}
