import LinkEditing from '@ckeditor/ckeditor5-link/src/linkediting'
import AutoLink from '@ckeditor/ckeditor5-link/src/autolink'
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview'
import unLinkIcon from '@ckeditor/ckeditor5-link/theme/icons/unlink.svg'
import ClickObserver from '@ckeditor/ckeditor5-engine/src/view/observer/clickobserver'
import Plugin from '@ckeditor/ckeditor5-core/src/plugin'

const TOOBAR_BUTTON_UNLINK = 'customUnlink'

export default class CustomUnLink extends Plugin {
    static get requires() {
        return [LinkEditing, AutoLink]
    }
    static get pluginName() {
        return 'CustomUnlink'
    }

    init() {

        this.editor.editing.view.addObserver(ClickObserver)

        // Create toolbar unlink buttons.
        this._createToolbarUnLinkButton()
    }

    _createToolbarUnLinkButton() {
        const editor = this.editor
        const linkCommand = editor.commands.get('link')
        const translate = editor.t
        const editingDocument = editor.editing.view.document

        editor.ui.componentFactory.add(TOOBAR_BUTTON_UNLINK, (locale) => {
            const button = new ButtonView(locale)

            button.isEnabled = true
            button.label = translate('Custom UnLink')
            button.icon = unLinkIcon
            button.tooltip = true
            button.isToggleable = true
            button.isVisible = true
            button.class = 'custom-unlink'

            // The unlink button will be enable if a link is enabled and focused
            button
                .bind('isEnabled')
                .to(
                    linkCommand,
                    'value',
                    editingDocument,
                    'isFocused',
                    (isCommandEnabled, isDocumentFocused) => isCommandEnabled && isDocumentFocused,
                )

            // The unlink button will be hidden if a link is not selected
            button.bind('isVisible').to(linkCommand, 'value', (value) => value)

            this.listenTo(button, 'execute', () => {
                editor.execute('unlink')
            })

            return button
        })
    }
}
