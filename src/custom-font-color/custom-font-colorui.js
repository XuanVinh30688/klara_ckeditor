import CustomColorUI from "./custom-colorui"
import { FONT_COLOR } from '@ckeditor/ckeditor5-font/src/utils';
import fontColorIcon from '@ckeditor/ckeditor5-font/theme/icons/font-color.svg';

export default class CustomFontColorUI extends CustomColorUI {

	constructor( editor ) {

		const t = editor.locale.t;
        
		super( editor, {
			commandName: FONT_COLOR,
			componentName: FONT_COLOR,
			icon: fontColorIcon,
			dropdownLabel: t( 'Font Color' )
		} );
	}

	static get pluginName() {
		return 'CustomFontColorUI';
	}
}