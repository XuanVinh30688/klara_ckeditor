 import { createDropdown } from '@ckeditor/ckeditor5-ui/src/dropdown/utils';
 import { normalizeColorOptions, getLocalizedColorOptions } from '@ckeditor/ckeditor5-ui/src/colorgrid/utils';
 import { addColorTableToDropdown } from '@ckeditor/ckeditor5-font/src/utils';
 import ColorUI from '@ckeditor/ckeditor5-font/src/ui/colorui';
 export default class CustomColorUI extends ColorUI {
     init() {
         const editor = this.editor;
         const locale = editor.locale;
         const t = locale.t;
         const command = editor.commands.get( this.commandName );
         const colorsConfig = normalizeColorOptions( editor.config.get( this.componentName ).colors );
         const localizedColors = getLocalizedColorOptions( locale, colorsConfig );
         const documentColorsCount = editor.config.get( `${ this.componentName }.documentColors` );
 
         // Register the UI component.
         editor.ui.componentFactory.add( this.componentName, locale => {
             const dropdownView = createDropdown( locale );
             this.colorTableView = addColorTableToDropdown( {
                 dropdownView,
                 colors: localizedColors.map( option => ( {
                     label: option.label,
                     color: option.model,
                     options: {
                         hasBorder: option.hasBorder
                     }
                 } ) ),
                 columns: this.columns,
                 removeButtonLabel: t( 'Remove color' ),
                 documentColorsLabel: documentColorsCount !== 0 ? t( 'Document colors' ) : undefined,
                 documentColorsCount: documentColorsCount === undefined ? this.columns : documentColorsCount
             } );
 
             this.colorTableView.bind( 'selectedColor' ).to( command, 'value' );
 
             dropdownView.buttonView.set( {
                 label: this.dropdownLabel,
                 icon: this.icon,
                 tooltip: true
             } );
 
             dropdownView.extendTemplate( {
                 attributes: {
                     class: 'ck-color-ui-dropdown'
                 }
             } );
 
             dropdownView.bind( 'isEnabled' ).to( command );
 
             dropdownView.on( 'execute', ( evt, data ) => {
                 editor.execute( this.commandName, data );
                 editor.editing.view.focus();

                // Custom code: Set selected font color variable. It is used for styling
                 const selectedColor = this.colorTableView.selectedColor ? this.colorTableView.selectedColor : 'var(--ck-color-base-text)';
                 dropdownView && dropdownView.element.style.setProperty('--ck-selected-font-color', selectedColor);
                 // End custom code
             } );
 
             dropdownView.on( 'change:isOpen', ( evt, name, isVisible ) => {
             
                 // Grids rendering is deferred (#6192).
                 dropdownView.colorTableView.appendGrids();
 
                 if ( isVisible ) {
                     if ( documentColorsCount !== 0 ) {
                         this.colorTableView.updateDocumentColors( editor.model, this.componentName );
                     }
                     this.colorTableView.updateSelectedColors();
                 }
             } );

             // Custom code: Hanlde update color when user click on document view
             const viewDocument = editor.editing.view.document
             
             this.listenTo(
                 viewDocument,
                 'click',
                 () => {
                     const selectedColor = this.colorTableView.selectedColor ? this.colorTableView.selectedColor : 'var(--ck-color-base-text)';
                     dropdownView.element.style.setProperty('--ck-selected-font-color', selectedColor);
                 },
                 {priority: 'highest'}
             )
             // End custom code

             return dropdownView;
         } );
     }
 }
 