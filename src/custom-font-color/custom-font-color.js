 import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
 import FontColorEditing from '@ckeditor/ckeditor5-font/src/fontcolor/fontcolorediting';
 import CustomFontColorUI from './custom-font-colorui';
 export default class CustomFontColor extends Plugin {
     static get requires() {
         return [ FontColorEditing, CustomFontColorUI ];
     }
 
     static get pluginName() {
         return 'CustomFontColor';
     }
 }