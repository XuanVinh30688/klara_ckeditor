/*
** This class is using for testing the functional of custom link
*/
export default class InsertLinkUI {
    constructor() {
        this.init()
    }

    init() {
        this.bindEvents()
    }

    bindEvents() {

        document.getElementById('btn-submit').addEventListener('click', () => {
            const url = document.getElementById('url-link').value
            this.trigger('link-change', url)
            this.hideUI()
        })

        document.getElementById('btn-cancel').addEventListener('click', () => {
            this.trigger('link-cancel')
            this.hideUI()
        })
    }

    showUI() {
        var inputLink = document.getElementById('input-link')
        var overlay = document.getElementById('overlay')

        inputLink.style.display = 'flex'
        overlay.style.display = 'block'
    }

    hideUI() {
        var inputLink = document.getElementById('input-link')
        var overlay = document.getElementById('overlay')

        inputLink.style.display = 'none'
        overlay.style.display = 'none'
    }

    trigger(eventName, data = {}) {
        const event = new CustomEvent(eventName, { detail: data })
        window.parent.dispatchEvent(event)
    }
}
